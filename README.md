# Vue Emali Validator

## Introduction

> Vue component for customized email validation. 
> Features
+ Check that email is from top-level domain (1500+ top-level domain) ( no more mail like ”@gmail.xx” or ”@gmail.com1” – check all top-level domains)
+ No more email from temporary mail providers (Blocks 3000+ temporary email providers)
+ Allow to whitelist email from domain (example: you can allow all mail from “10minutemail.info”)
+ Allow to block email from domain (example: you can block all mail from “gmail.com”)
+ Detailed response format
+ Easy to configure

## Requirement
+ VueJS
+ Bootstrap Vue
+ Lodash

## Code Samples

> Import `ValidatedInputEmail` component and use as below

``` bash
<validated-input-email
                  id= "test"
                  placeholder= "Enter email"
                  filter-top-level-domain= true
                  filter-temp-email-providers= true
                  :whitelisted-domains= "whitelisted_domains"
                  :blocked-domains= "blocked_domains"
                  v-on:response= "validateResponse"
                  ></validated-input-email>

```

## Installation

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
## Configuration
### Parameters 
+ `filter-top-level-domain` - values `true` / `false`
+ `filter-temp-email-providers` - values `true` / `false`
+ `whitelisted-domains` - array of domains to be whitelisted
    + `['1to1mail.org', 'asd234.com']`
+ `blocked-domains` - array of domains to be blocked
    + `['1to1mail.org', 'asd234.com']`
    
## Callback event
`response` event is triggered on email validation. 

## Response Format
> All responses are in JSON with 3 parameters
+ `status_code`
  + 0 - Valid email
  + 1 - Email has invalid format
  + 2 - Email is part of blocked list
  + 3 - Email is provided by temporary email service provider
  + 4 - Not a top domain email
+ `status_message` - helper messages for each status code
  + status_code = 0, "Valid Email"
  + status_code = 1, "Invalid Format"
  + status_code = 2, "Blocked Email"
  + status_code = 3, "Temp domain email"
  + status_code = 4, "Not Top domain email"
+ `data` - email entered by user
### Sample Response
``` json
{
    data : "test@yahoo.com"
    status_code : 0
    status_message : "Valid Email"
}
```
